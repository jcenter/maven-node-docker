FROM wallbase/node:14.17.1-alpine-docker

MAINTAINER  wangkun23 <845885222@qq.com>
#RUN echo https://mirrors.aliyun.com/alpine/v3.11/main > /etc/apk/repositories && \
#    echo https://mirrors.aliyun.com/alpine/v3.11/community >> /etc/apk/repositories
RUN apk update && apk upgrade
RUN apk add git openjdk8 maven --no-cache \
    && rm -rf /var/cache/apk/*
